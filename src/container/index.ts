import { container } from 'tsyringe';

import MailEventsRepository from '../repositories/implementations/MailEventsRepository';
import IMailEventsRepository from '../repositories/interfaces/IMailEventsRepository';

import SacadosRepository from '../repositories/implementations/SacadosRepository';
import ISacadosRepository from '../repositories/interfaces/ISacadosRepository';

import TitlesRepository from '../repositories/implementations/TitlesRepository';
import ITitlesRepository from '../repositories/interfaces/ITitlesRepository';

import OperationsRepository from '../repositories/implementations/OperationsRepository';
import IOperationsRepository from '../repositories/interfaces/IOperationsRepository';

container.registerSingleton<IMailEventsRepository>(
  'MailEventsRepository',
  MailEventsRepository,
);

container.registerSingleton<ISacadosRepository>(
  'SacadosRepository',
  SacadosRepository,
);

container.registerSingleton<ITitlesRepository>(
  'TitlesRepository',
  TitlesRepository,
);

container.registerSingleton<IOperationsRepository>(
  'OperationsRepository',
  OperationsRepository,
);
