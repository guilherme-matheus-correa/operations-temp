import { inject, injectable } from 'tsyringe';

import { IMailEvent } from '../entities/MailEvent';
import IMailEventsRepository from '../repositories/interfaces/IMailEventsRepository';
import ISacadosRepository from '../repositories/interfaces/ISacadosRepository';

interface IRequest {
  mailEvents: IMailEvent[];
}

@injectable()
class CreateMailEventsService {
  constructor(
    @inject('MailEventsRepository')
    private mailEventsRepository: IMailEventsRepository,
    @inject('SacadosRepository')
    private sacadosRepository: ISacadosRepository,
  ) {}

  public async execute({ mailEvents }: IRequest): Promise<void> {
    mailEvents.map(async (mailEvent: IMailEvent) => {
      const {
        webhookId,
        email,
        event,
        sendgridMessageId,
        timestamp,
        reason,
      } = mailEvent;

      if (!webhookId) {
        await this.mailEventsRepository.create({
          webhookId,
          email,
          event,
          sendgridMessageId,
          timestamp,
          reason,
        });
      } else {
        const sacadoExists = await this.sacadosRepository.findByWebhookId(
          webhookId,
        );

        if (sacadoExists) {
          Object.assign(sacadoExists, sendgridMessageId);

          await this.sacadosRepository.update(sacadoExists);
        }

        await this.mailEventsRepository.create({
          webhookId,
          email,
          event,
          sendgridMessageId,
        });
      }
    });
  }
}

export default CreateMailEventsService;
