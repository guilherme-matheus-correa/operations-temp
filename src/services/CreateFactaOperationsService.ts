import { inject, injectable } from 'tsyringe';
import { format } from 'date-fns';
import axios from 'axios';
import generateSacadoNotificationHtml from 'utils/generateSacadoNotificationHtml';
import generateTitleHtml from '../utils/generateTitleHtml';
import IBorderoDTO from '../dtos/IBorderoDTO';
import IOperationsRepository from '../repositories/interfaces/IOperationsRepository';
import ISacadosRepository from '../repositories/interfaces/ISacadosRepository';
import ITitlesRepository from '../repositories/interfaces/ITitlesRepository';
import AppObjectError from '../errors/AppObjectError';

interface ISacadoNotification {
  webhookId: string;
  email: string;
  subject: string;
  htmlContent: string;
}

interface IRequest {
  borderos: IBorderoDTO[];
}

@injectable()
class CreateFactaOperationsService {
  constructor(
    @inject('OperationsRepository')
    private operationsRepository: IOperationsRepository,
    @inject('SacadosRepository')
    private sacadosRepository: ISacadosRepository,
    @inject('TitlesRepository')
    private titlesRepository: ITitlesRepository,
  ) {}

  public async execute({ borderos }: IRequest): Promise<void> {
    const sacadosNotificationList: ISacadoNotification[] = [];

    for (
      let borderoIterator = 0;
      borderoIterator < borderos.length;
      borderoIterator += 1
    ) {
      const borderoItem = borderos[borderoIterator];

      const {
        bordero,
        cedente,
        cedenteId,
        avalista,
        avalistaId,
        dateProcessing,
        sacados,
      } = borderoItem;

      const operation = await this.operationsRepository.create({
        bordero,
        cedente,
        cedenteId,
        avalista,
        avalistaId,
        dateProcessing,
      });

      const { _id: operationId } = operation;

      for (
        let sacadoIterator = 0;
        sacadoIterator < sacados.length;
        sacadoIterator += 1
      ) {
        const sacadoItem = sacados[sacadoIterator];

        const { sacadoFactaId, name, emails, titles } = sacadoItem;

        if (emails.trim()) {
          const splittedEmails = emails.split(';');
          for (
            let emailIterator = 0;
            emailIterator < splittedEmails.length;
            emailIterator += 1
          ) {
            const email = splittedEmails[emailIterator];

            const isBorderoSaca = await this.sacadosRepository.findByBorderoAndSacadoFactaId(
              {
                bordero,
                sacadoFactaId,
              },
            );

            const webhookId = `${bordero}_${sacadoFactaId.replace(
              /[^\d]/g,
              '',
            )}_${isBorderoSaca.length + 1}_${format(new Date(), 'yyyy_MM_dd')}`;

            const sacadoJson = JSON.stringify(sacadoItem);

            const sacado = await this.sacadosRepository.create({
              bordero,
              cedente,
              cedenteId,
              name,
              sacadoFactaId,
              operationId,
              email,
              dateProcessing: new Date(),
              json: sacadoJson,
              webhookId,
            });

            const { _id: sacadoId } = sacado;

            let titlesHtmlContent = '';

            for (
              let titleIterator = 0;
              titleIterator < titles.length;
              titleIterator += 1
            ) {
              const titleItem = titles[titleIterator];

              const {
                // cedente: titleCedente,
                avalista: titleAvalista,
                titleData,
              } = titleItem;

              const {
                titleDate,
                titleId,
                titleValue,
                dateDocument,
                creationDate,
                documentNumber,
                ourNumber,
              } = titleData;

              const titleJson = JSON.stringify(titleItem);

              await this.titlesRepository.create({
                operationId,
                sacadoId,
                documentNumber,
                titleId,
                titleDate,
                titleValue,
                ourNumber,
                creationDate,
                dateDocument,
                json: titleJson,
              });

              titlesHtmlContent += generateTitleHtml({
                titleDate,
                titleValue,
                documentNumber,
              });

              const subject = `Notificação+Boleto: Nr. Docto: ${sacadoItem.titles[0].titleData.documentNumber} Ref: ${titleAvalista} Sacado: ${name}`;

              const htmlContent = generateSacadoNotificationHtml({
                cedente,
                cedenteId,
                name,
                sacadoFactaId,
                titlesHtmlContent,
              });

              const sacadoNotification: ISacadoNotification = {
                webhookId,
                email,
                subject,
                htmlContent,
              };

              sacadosNotificationList.push(sacadoNotification);
            }
          }
        } else {
          const isBorderoSaca = await this.sacadosRepository.findByBorderoAndSacadoFactaId(
            {
              bordero,
              sacadoFactaId,
            },
          );

          const webhookId = `${bordero}_${sacadoFactaId.replace(
            /[^\d]/g,
            '',
          )}_${isBorderoSaca.length + 1}_${format(new Date(), 'yyyy_MM_dd')}`;

          const sacadoJson = JSON.stringify(sacadoItem);

          const sacado = await this.sacadosRepository.create({
            bordero,
            cedente,
            cedenteId,
            name,
            sacadoFactaId,
            operationId,
            email: '',
            dateProcessing: new Date(),
            json: sacadoJson,
            webhookId,
          });

          const { _id: sacadoId } = sacado;

          for (
            let titleIterator = 0;
            titleIterator < titles.length;
            titleIterator += 1
          ) {
            const titleItem = titles[titleIterator];

            const { titleData } = titleItem;

            const {
              titleDate,
              titleId,
              titleValue,
              dateDocument,
              creationDate,
              documentNumber,
              ourNumber,
            } = titleData;

            const titleJson = JSON.stringify(titleItem);

            await this.titlesRepository.create({
              operationId,
              sacadoId,
              documentNumber,
              titleId,
              titleDate,
              titleValue,
              ourNumber,
              creationDate,
              dateDocument,
              json: titleJson,
            });
          }
        }
      }
    }

    try {
      await axios.post(
        `http://127.0.0.1:3000/send-mail-sendgrid/`,
        {
          sacadosNotificationList,
        },
        {
          maxContentLength: Infinity,
          maxBodyLength: Infinity,
        },
      );
    } catch (error) {
      throw new AppObjectError({
        error: error.message,
        sacadosNotificationList,
      });
    }
  }
}

export default CreateFactaOperationsService;
