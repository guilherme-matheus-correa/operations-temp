import { inject, injectable } from 'tsyringe';

import { IOperation } from '../entities/Operation';
import { ISacado } from '../entities/Sacado';
import { IMailEvent } from '../entities/MailEvent';
import IOperationsRepository from '../repositories/interfaces/IOperationsRepository';
import ISacadosRepository from '../repositories/interfaces/ISacadosRepository';
import IMailEventsRepository from '../repositories/interfaces/IMailEventsRepository';

interface ISacadoWithMailEvents extends ISacado {
  mailEvents: IMailEvent[];
}

interface IOperationWithSacadoAndMailEvents extends IOperation {
  sacados: ISacadoWithMailEvents[];
}

interface IResponse {
  operations: IOperationWithSacadoAndMailEvents[];
  total: number;
}

interface IRequest {
  page: number;
  perPage: number;
}

@injectable()
class ListOperationsWithSacadosAndMailEventsService {
  constructor(
    @inject('OperationsRepository')
    private operationsRepository: IOperationsRepository,
    @inject('SacadosRepository')
    private sacadosRepository: ISacadosRepository,
    @inject('MailEventsRepository')
    private mailEventsRepository: IMailEventsRepository,
  ) {}

  public async execute({
    page = 0,
    perPage = 25,
  }: IRequest): Promise<IResponse> {
    const {
      operations,
      total,
    } = await this.operationsRepository.findAndCountWithPagination({
      page,
      perPage,
    });

    const operationsWithSacadoAndMailEvents: IOperationWithSacadoAndMailEvents[] = [];

    for (
      let operationIterator = 0;
      operationIterator < operations.length;
      operationIterator += 1
    ) {
      const operation = operations[operationIterator];

      const { _id: operationId } = operation;

      const sacados = await this.sacadosRepository.findByOperationId(
        operationId,
      );

      const sacadosWithMailEvents: ISacadoWithMailEvents[] = [];

      for (
        let sacadoIterator = 0;
        sacadoIterator < sacados.length;
        sacadoIterator += 1
      ) {
        const sacado = sacados[sacadoIterator];

        const { webhookId } = sacado;

        const mailEvents = await this.mailEventsRepository.findByWebhookId(
          webhookId,
        );

        sacadosWithMailEvents.push({ ...sacado.toObject(), mailEvents });
      }

      operationsWithSacadoAndMailEvents.push({
        ...operation.toObject(),
        sacados: sacadosWithMailEvents,
      });
    }

    return { operations: operationsWithSacadoAndMailEvents, total };
  }
}

export default ListOperationsWithSacadosAndMailEventsService;
