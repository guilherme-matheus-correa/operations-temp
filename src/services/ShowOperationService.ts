import { inject, injectable } from 'tsyringe';

import AppError from 'errors/AppError';
import { IOperation } from '../entities/Operation';
import { ISacado } from '../entities/Sacado';
import { IMailEvent } from '../entities/MailEvent';
import IOperationsRepository from '../repositories/interfaces/IOperationsRepository';
import ISacadosRepository from '../repositories/interfaces/ISacadosRepository';
import IMailEventsRepository from '../repositories/interfaces/IMailEventsRepository';

interface ISacadoWithMailEvents extends ISacado {
  mailEvents: IMailEvent[];
}

interface IOperationWithSacadoAndMailEvents extends IOperation {
  sacados?: ISacadoWithMailEvents[];
}

interface IResponse {
  operation: IOperationWithSacadoAndMailEvents;
}

interface IRequest {
  id: string;
}

@injectable()
class ListOperationsWithSacadosAndMailEventsService {
  constructor(
    @inject('OperationsRepository')
    private operationsRepository: IOperationsRepository,
    @inject('SacadosRepository')
    private sacadosRepository: ISacadosRepository,
    @inject('MailEventsRepository')
    private mailEventsRepository: IMailEventsRepository,
  ) {}

  public async execute({ id }: IRequest): Promise<IResponse> {
    const operation = await this.operationsRepository.findById(id);

    if (!operation) {
      throw new AppError('Operation not found');
    }

    const sacados = await this.sacadosRepository.findByOperationId(id);

    const sacadosWithMailEvents: ISacadoWithMailEvents[] = [];

    for (
      let sacadoIterator = 0;
      sacadoIterator < sacados.length;
      sacadoIterator += 1
    ) {
      const sacado = sacados[sacadoIterator];

      const { _id: sacadoId } = sacado;

      const mailEvents = this.mailEventsRepository.findBySacadoId(sacadoId);

      sacadosWithMailEvents.push({ ...sacado.toObject(), mailEvents });
    }

    const operationWithSacadoAndMailEvents: IOperationWithSacadoAndMailEvents = {
      ...operation.toObject(),
      sacados: sacadosWithMailEvents,
    };

    return { operation: operationWithSacadoAndMailEvents };
  }
}

export default ListOperationsWithSacadosAndMailEventsService;
