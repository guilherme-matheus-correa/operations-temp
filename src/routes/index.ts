import { Router } from 'express';

import MailEventsController from '../controller/MailEventsController';

const routes = Router();

const mailEventsController = new MailEventsController();

routes.use('/mail-events', mailEventsController.store);

export default routes;
