import ITitleDTO from './ITitleDTO';

export default interface ISacadoDTO {
  sacadoFactaId: string;
  name: string;
  emails: string;
  titles: ITitleDTO[];
}
