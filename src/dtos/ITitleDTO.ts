interface ITitleData {
  documentNumber: string;
  titleId: string;
  titleDate: Date;
  titleValue: number;
  ourNumber: string;
  creationDate: Date;
  dateDocument: Date;
}

export default interface ITitle {
  cedente: string;
  avalista: string;
  titleData: ITitleData;
}
