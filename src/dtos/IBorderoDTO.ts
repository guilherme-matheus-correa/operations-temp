import ISacadoDTO from './ISacadoDTO';

export default interface IBordero {
  bordero: number;
  cedente: string;
  cedenteId: string;
  avalistaId: string;
  avalista: string;
  dateProcessing: Date;
  sacados: ISacadoDTO[];
}
