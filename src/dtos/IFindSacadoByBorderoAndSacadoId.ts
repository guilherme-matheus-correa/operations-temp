export default interface IFindSacadoByBorderoAndSacadoId {
  bordero: number;
  sacadoFactaId: string;
}
