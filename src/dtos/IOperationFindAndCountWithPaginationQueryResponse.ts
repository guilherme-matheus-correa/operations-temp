import { IOperation } from '../entities/Operation';

export default interface IOperationFindAndCountWithPaginationQueryResponse {
  operations: IOperation[];
  total: number;
}
