export default interface ICreateOperationDTO {
  bordero: number;
  cedenteId: string;
  cedente: string;
  avalistaId: string;
  avalista: string;
  dateProcessing: Date;
}
