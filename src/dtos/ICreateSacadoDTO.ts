import { IOperation } from '../entities/Operation';

export default interface ICreateSacadoDTO {
  operationId: IOperation['_id'];
  dateProcessing: Date;
  webhookId: string;
  sendgridMessageId?: string;
  sacadoFactaId: string;
  bordero: number;
  name: string;
  email?: string;
  cedenteId: string;
  cedente: string;
  json: string;
}
