export default interface ICreateMailEventDTO {
  webhookId?: string;
  email: string;
  event?: string;
  sendgridMessageId?: string;
  timestamp?: Date;
  reason?: string;
}
