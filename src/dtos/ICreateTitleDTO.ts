import { IOperation } from '../entities/Operation';
import { ISacado } from '../entities/Sacado';

export default interface ICreateTitleDTO {
  operationId: IOperation['_id'];
  sacadoId: ISacado['_id'];
  documentNumber: string;
  titleId: string;
  titleDate: Date;
  titleValue: number;
  ourNumber: string;
  creationDate: Date;
  dateDocument: Date;
  json: string;
}
