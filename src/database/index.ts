import mongoose from 'mongoose';

class Database {
  uri: string;

  constructor() {
    this.uri =
      process.env.MONGO_URL || 'mongodb://localhost:27017/template-typescript';
    mongoose.connect(this.uri, {
      useCreateIndex: true,
      useNewUrlParser: true,
      useFindAndModify: true,
      useUnifiedTopology: true,
    });
  }
}

export default new Database();
