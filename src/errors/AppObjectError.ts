class AppObjectError {
  public readonly message: object;

  public readonly statusCode: number;

  constructor(message: object, statusCode = 400) {
    this.message = message;
    this.statusCode = statusCode;
  }
}

export default AppObjectError;
