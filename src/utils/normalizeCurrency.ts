const normalizeCurrency = (value: number): string => {
  const stringOfNumber = String(value);
  const cents =
    stringOfNumber[stringOfNumber.length - 2] +
    stringOfNumber[stringOfNumber.length - 1];
  const reals = stringOfNumber.slice(0, stringOfNumber.length - 2);
  let pointValue = '';

  const arrayNumber = reals.split('');
  arrayNumber.reverse();
  arrayNumber.forEach((number, index) => {
    if ((index + 1) % 3 === 0) {
      if (index === arrayNumber.length - 1) {
        pointValue = number + pointValue;
      } else {
        pointValue = `.${number}${pointValue}`;
      }
    } else {
      pointValue = number + pointValue;
    }
  });
  return `${pointValue},${cents}`;
};

export default normalizeCurrency;
