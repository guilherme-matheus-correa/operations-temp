import { format } from 'date-fns';
import normalizeCurrency from './normalizeCurrency';

interface IGeneretaTitleHtmlArguments {
  titleDate: Date;
  titleValue: number;
  documentNumber: string;
}

export default function generateTitleHtml({
  titleDate,
  titleValue,
  documentNumber,
}: IGeneretaTitleHtmlArguments): string {
  return `
  <div>
    <span>
      VENCIMENTO: ${format(new Date(titleDate), 'dd/MM/yyyy')}<br />
      VALOR: R$ ${normalizeCurrency(titleValue)}<br />
      DOCUMENTO: ${documentNumber}<br />
    </span>
    <br/>
  </div> `;
}
