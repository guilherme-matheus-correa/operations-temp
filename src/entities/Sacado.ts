import mongoose, { Schema, Document } from 'mongoose';

import { IOperation } from './Operation';

export interface ISacado extends Document {
  operationId: IOperation['_id'];
  dateProcessing: Date;
  webhookId: string;
  sendgridMessageId?: string;
  sacadoFactaId: string;
  bordero: number;
  name: string;
  email?: string;
  cedenteId: string;
  cedente: string;
  json: string;
}

const SacadoSchema: Schema = new mongoose.Schema(
  {
    operationId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'operations',
    },
    dateProcessing: {
      type: Date,
      required: true,
    },
    webhookId: {
      type: String,
      required: true,
    },
    sendgridMessageId: {
      type: String,
    },
    sacadoFactaId: {
      type: String,
      required: true,
    },
    bordero: {
      type: Number,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
    },
    cedenteId: {
      type: String,
      required: true,
    },
    cedente: {
      type: String,
      required: true,
    },
    json: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  },
);

export default mongoose.model<ISacado>('sacados', SacadoSchema);
