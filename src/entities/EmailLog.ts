import mongoose, { Schema, Document } from 'mongoose';

export interface IEmailLog extends Document {
  sacadoId: mongoose.Schema.Types.ObjectId;
  email: string;
}

const EmailLogSchema: Schema = new mongoose.Schema(
  {
    sacadoId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'sacados',
    },
    email: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  },
);

export default mongoose.model<IEmailLog>('emails', EmailLogSchema);
