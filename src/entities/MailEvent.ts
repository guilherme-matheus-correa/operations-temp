import mongoose, { Schema, Document } from 'mongoose';

export interface IMailEvent extends Document {
  webhookId?: string;
  email: string;
  event?: string;
  sendgridMessageId?: string;
  timestamp?: Date;
  reason?: string;
}

const MailEventSchema: Schema = new mongoose.Schema(
  {
    webhookId: {
      type: String,
    },
    email: {
      type: String,
      required: true,
    },
    event: {
      type: String,
    },
    sendgridMessageId: {
      type: String,
    },
    timestamp: {
      type: Date,
    },
    reason: {
      type: String,
    },
  },
  {
    timestamps: true,
  },
);

export default mongoose.model<IMailEvent>('mailEvents', MailEventSchema);
