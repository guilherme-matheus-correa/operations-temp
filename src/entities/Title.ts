import mongoose, { Schema, Document } from 'mongoose';

import { IOperation } from './Operation';
import { ISacado } from './Sacado';

export interface ITitle extends Document {
  operationId: IOperation['_id'];
  sacadoId: ISacado['_id'];
  documentNumber: string;
  titleId: string;
  titleDate: Date;
  titleValue: number;
  ourNumber: string;
  creationDate: Date;
  dateDocument: Date;
  json: string;
}

const TitleSchema: Schema = new mongoose.Schema(
  {
    operationId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'operations',
    },
    sacadoId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'sacados',
    },
    documentNumber: {
      type: String,
      required: true,
    },
    titleId: {
      type: String,
      required: true,
    },
    titleDate: {
      type: Date,
      required: true,
    },
    titleValue: {
      type: Number,
      required: true,
    },
    ourNumber: {
      type: String,
      required: true,
    },
    creationDate: {
      type: Date,
      required: true,
    },
    dateDocument: {
      type: Date,
      required: true,
    },
    json: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  },
);

export default mongoose.model<ITitle>('titles', TitleSchema);
