import mongoose, { Schema, Document } from 'mongoose';

export interface IOperation extends Document {
  bordero: number;
  cedenteId: string;
  cedente: string;
  avalistaId: string;
  avalista: string;
  dateProcessing: Date;
}

const OperationSchema: Schema = new mongoose.Schema(
  {
    bordero: {
      type: Number,
      required: true,
    },
    cedenteId: {
      type: String,
      required: true,
    },
    cedente: {
      type: String,
      required: true,
    },
    avalistaId: {
      type: String,
      required: true,
    },
    avalista: {
      type: String,
      required: true,
    },
    dateProcessing: {
      type: Date,
      required: true,
    },
  },
  {
    timestamps: true,
  },
);

export default mongoose.model<IOperation>('operations', OperationSchema);
