import { ITitle } from '../../entities/Title';

import ICreateTitleDTO from '../../dtos/ICreateTitleDTO';

export default interface ITitleRepository {
  create(data: ICreateTitleDTO): Promise<ITitle>;
  findBySacadoId(sacadoId: string): Promise<ITitle | null>;
}
