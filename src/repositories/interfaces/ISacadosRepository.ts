import { ISacado } from '../../entities/Sacado';

import ICreateSacadoDTO from '../../dtos/ICreateSacadoDTO';
import IFindSacadoByBorderoAndSacadoId from '../../dtos/IFindSacadoByBorderoAndSacadoId';

export default interface ISacadoRepository {
  create(data: ICreateSacadoDTO): Promise<ISacado>;
  findAll(): Promise<ISacado[]>;
  findByWebhookId(webhookId: string): Promise<ISacado | null>;
  findByBorderoAndSacadoFactaId({
    bordero,
    sacadoFactaId,
  }: IFindSacadoByBorderoAndSacadoId): Promise<ISacado[]>;
  findByOperationId(operationId: string): Promise<ISacado[]>;
  update(sacado: ISacado): Promise<ISacado>;
}
