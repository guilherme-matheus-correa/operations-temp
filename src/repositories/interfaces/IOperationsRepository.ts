import { IOperation } from '../../entities/Operation';

import ICreateOperationDTO from '../../dtos/ICreateOperationDTO';
import IFindAndCountOperationsWithPaginationDTO from '../../dtos/IFindAndCountOperationsWithPaginationDTO';
import IOperationFindAndCountWithPaginationQueryResponse from '../../dtos/IOperationFindAndCountWithPaginationQueryResponse';

export default interface IOperationRepository {
  create(data: ICreateOperationDTO): Promise<IOperation>;
  findAndCountWithPagination(
    findData: IFindAndCountOperationsWithPaginationDTO,
  ): Promise<IOperationFindAndCountWithPaginationQueryResponse>;
  findById(id: string): Promise<IOperation | null>;
  findByBordero(bordero: number): Promise<IOperation | null>;
  findByCedenteId(cedenteId: string): Promise<IOperation[]>;
}
