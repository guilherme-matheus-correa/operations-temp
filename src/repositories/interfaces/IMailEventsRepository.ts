import { IMailEvent } from '../../entities/MailEvent';

import ICreateMailEventDTO from '../../dtos/ICreateMailEventDTO';

export default interface IMailEventsRepository {
  create(data: ICreateMailEventDTO): Promise<IMailEvent>;
  findAll(): Promise<IMailEvent[]>;
  findByWebhookId(webhookId: string): Promise<IMailEvent[]>;
  findBySacadoId(sacadoId: string): Promise<IMailEvent[]>;
}
