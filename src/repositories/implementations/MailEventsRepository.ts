import { Model } from 'mongoose';
import ICreateMailEventDTO from '../../dtos/ICreateMailEventDTO';
import IMailEventsRepository from '../interfaces/IMailEventsRepository';
import MailEvent, { IMailEvent } from '../../entities/MailEvent';

class MailEventsRepository implements IMailEventsRepository {
  private mongoRepository: Model<IMailEvent, {}>;

  constructor() {
    this.mongoRepository = MailEvent;
  }

  public async create(data: ICreateMailEventDTO): Promise<IMailEvent> {
    return this.mongoRepository.create(data);
  }

  public async findAll(): Promise<IMailEvent[]> {
    return this.mongoRepository.find();
  }

  public async findByWebhookId(webhookId: string): Promise<IMailEvent[]> {
    return this.mongoRepository.find({ webhookId });
  }

  public async findBySacadoId(sacadoId: string): Promise<IMailEvent[]> {
    return this.mongoRepository.find({ sacadoId });
  }
}

export default MailEventsRepository;
