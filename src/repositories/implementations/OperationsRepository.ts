import { Model } from 'mongoose';
import ICreateOperationDTO from '../../dtos/ICreateOperationDTO';
import IFindAndCountOperationsWithPaginationDTO from '../../dtos/IFindAndCountOperationsWithPaginationDTO';
import IOperationFindAndCountWithPaginationQueryResponse from '../../dtos/IOperationFindAndCountWithPaginationQueryResponse';
import IOperationsRepository from '../interfaces/IOperationsRepository';
import Operation, { IOperation } from '../../entities/Operation';

class OperationsRepository implements IOperationsRepository {
  private mongoRepository: Model<IOperation, {}>;

  constructor() {
    this.mongoRepository = Operation;
  }

  public async create(data: ICreateOperationDTO): Promise<IOperation> {
    return this.mongoRepository.create(data);
  }

  public async findAndCountWithPagination({
    page,
    perPage,
  }: IFindAndCountOperationsWithPaginationDTO): Promise<
    IOperationFindAndCountWithPaginationQueryResponse
  > {
    const operations = await this.mongoRepository
      .find()
      .limit(perPage)
      .skip(perPage * page)
      .sort({ dateProcessing: -1 });

    const total = await this.mongoRepository.countDocuments({});

    return { operations, total };
  }

  public async findById(id: string): Promise<IOperation | null> {
    return this.mongoRepository.findById(id);
  }

  public async findByBordero(bordero: number): Promise<IOperation | null> {
    return this.mongoRepository.findOne({ bordero });
  }

  public async findByCedenteId(cedenteId: string): Promise<IOperation[]> {
    return this.mongoRepository.find({ cedenteId });
  }
}

export default OperationsRepository;
