import { Model } from 'mongoose';
import ICreateTitleDTO from '../../dtos/ICreateTitleDTO';
import ITitlesRepository from '../interfaces/ITitlesRepository';
import Title, { ITitle } from '../../entities/Title';

class TitlesRepository implements ITitlesRepository {
  private mongoRepository: Model<ITitle, {}>;

  constructor() {
    this.mongoRepository = Title;
  }

  public async create(data: ICreateTitleDTO): Promise<ITitle> {
    return this.mongoRepository.create(data);
  }

  public async findBySacadoId(sacadoId: string): Promise<ITitle | null> {
    return this.mongoRepository.findOne({ sacadoId });
  }
}

export default TitlesRepository;
