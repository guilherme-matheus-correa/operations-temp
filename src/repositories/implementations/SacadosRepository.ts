import { Model } from 'mongoose';
import IFindSacadoByBorderoAndSacadoId from 'dtos/IFindSacadoByBorderoAndSacadoId';
import ICreateSacadoDTO from '../../dtos/ICreateSacadoDTO';
import ISacadosRepository from '../interfaces/ISacadosRepository';
import Sacado, { ISacado } from '../../entities/Sacado';

class SacadosRepository implements ISacadosRepository {
  private mongoRepository: Model<ISacado, {}>;

  constructor() {
    this.mongoRepository = Sacado;
  }

  public async create(data: ICreateSacadoDTO): Promise<ISacado> {
    return this.mongoRepository.create(data);
  }

  public async findAll(): Promise<ISacado[]> {
    return this.mongoRepository.find();
  }

  public async findByWebhookId(webhookId: string): Promise<ISacado | null> {
    return this.mongoRepository.findOne({ webhookId });
  }

  public async findByBorderoAndSacadoFactaId({
    bordero,
    sacadoFactaId,
  }: IFindSacadoByBorderoAndSacadoId): Promise<ISacado[]> {
    return this.mongoRepository.find({ bordero, sacadoFactaId });
  }

  public async findByOperationId(operationId: string): Promise<ISacado[]> {
    return this.mongoRepository.find({ operationId });
  }

  public async update(sacado: ISacado): Promise<ISacado> {
    return sacado.save();
  }
}

export default SacadosRepository;
