import { Request, Response } from 'express';
import axios from 'axios';
// import * as Yup from 'yup';
import { container } from 'tsyringe';
// import getValidationErrors from '../utils/getValidationErrors';
import CreateFactaOperationsService from '../services/CreateFactaOperationsService';
import IBorderoDTO from '../dtos/IBorderoDTO';

interface IGetDataFromFactaResponse {
  status: number;
  data?: IBorderoDTO[];
}

export default class OperationsController {
  public async store(request: Request, response: Response): Promise<Response> {
    async function getDataFromFacta(): Promise<IGetDataFromFactaResponse> {
      try {
        const { data } = await axios.get<IBorderoDTO[]>(
          'https://api.grupobfc.com.br/facta/charge',
        );

        return { status: 200, data };
      } catch (error) {
        return { status: 500 };
      }
    }

    const getDataFromFactaResponse = await getDataFromFacta();

    const { status, data } = getDataFromFactaResponse;

    if (status === 500) {
      return response.status(404).send();
    }

    if (!data) {
      return response.status(204).send();
    }

    const createFactaOperationsService = container.resolve(
      CreateFactaOperationsService,
    );

    await createFactaOperationsService.execute({
      borderos: data,
    });

    return response.status(200).json({
      message: 'Sacados operation notifications list emails was sent',
    });
  }
}
