import { Request, Response } from 'express';
import { container } from 'tsyringe';
import CreateMailEventsService from '../services/CreateMailEventsService';

export default class MailEventsController {
  public async store(request: Request, response: Response): Promise<Response> {
    const createMailEventsService = container.resolve(CreateMailEventsService);

    const mailEvents = request.body;

    await createMailEventsService.execute({
      mailEvents,
    });

    return response.status(201).json({ message: 'Events registered' });
  }
}
